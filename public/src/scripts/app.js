angular.module("AngularBaseApp", [
  'AngularBaseApp.directives',
  'AngularBaseApp.controllers',
  'AngularBaseApp.services',
  'AngularBaseApp.filters'
]);
