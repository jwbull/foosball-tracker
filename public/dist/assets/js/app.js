angular.module("AngularBaseApp", [
  'AngularBaseApp.directives',
  'AngularBaseApp.controllers',
  'AngularBaseApp.services',
  'AngularBaseApp.filters'
]);

(function () { 'use strict';

  var filters = angular.module('AngularBaseApp.filters', []);

  filters.filter('range', function() {
    return function(input, total) {
      total = parseInt(total);
      for (var i = 0; i < total; i++) {
        input.push(i);
      }
      return input;
    };
  });

  filters.filter('reverse', function() {
    return function(items) {
      return items.slice().reverse();
    };
  });

}());

(function () { 'use strict';

  var controllers = angular.module('AngularBaseApp.controllers', []);

  controllers.controller('MainController', ['$scope', function($scope) {
    // $scope.paths = {};
    // $scope.paths.views = {};
    // $scope.paths.views.common = '/views/common';
  }]);

}());

(function () { 'use strict';

  var directives = angular.module('AngularBaseApp.directives', []);

  directives.directive('grid', function() {
    return {
      restrict: 'E',
      templateUrl: 'views/common/grid.html'
    };
  });

}());

(function () { 'use strict';

  var services = angular.module('AngularBaseApp.services', []);

  services.factory('service', ['$window', function (win) {
    return function () {};
  }]);

}());
