# Foosball Tracker

Keep track of foosball game scores

## Installation Instructions

### Clone repository and install submodules
```
mkdir foosball-tracker
cd foosball-tracker
git remote add origin git@bitbucket.org:jwbull/foosball-tracker.git
git remote add angular-base git@bitbucket.org:jwbull/angular-base.git
git fetch --all
git checkout origin/master
git merge --squash angular-base/master
git submodule update --init
```

### Parse vagrant.yml to get Ansible vault information
```
. git_modules/vagrant-base/bin/parse_yaml.sh
eval $(parse_yaml vagrant.yml 'vagrant_')
```

### Output random password to Ansible vault file
```
eval vagrant_vault_file="${vagrant_vault_path}/${vagrant_host_name}"
openssl rand 32 -base64 -out $vagrant_vault_file
```

### Install Ansible dependencies
```
sudo ansible-galaxy install --force --role-file git_modules/vagrant-base/provisioning/requirements
```

### Provision Vagrant virtual machine and boot up
```
vagrant up && vagrant ssh
```

### Install project dependencies
```
cd /var/www
npm install
node_modules/.bin/bower install
node_modules/.bin/gulp-base
```